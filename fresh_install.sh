#!/bin/sh

alias pacman='pacman --noconfirm'

test $EUID -ne 0&&{ echo "Script must be run as root!"; exit 1; }

# Install aur manager if not already installed
if ! which trizen > /dev/null; then
  git clone https://aur.archlinux.org/trizen.git
  cd trizen && makepkg -si
fi

# Do a full refresh and system upgrade
pacman -Syyu

# Install required packages
pacman -S pipewire pipewire-pulse pavucontrol acpilight xorg kitty \
  pcmanfm opendoas pamixer exa bat helix zathura vimb bottom curl dash rnote \
  dunst keepassxc xsel speedcrunch

# Configs
echo -e 'SUBSYSTEM=="backlight", ACTION=="add"
  RUN+="/bin/chgrp video /sys/class/backlight/%k/brightness"
  RUN+="/bin/chmod g+w /sys/class/backlight/%k/brightness"' > /etc/udev/rules.d/90-backlight.rules

echo 'permit setenv { XAUTHORITY LANG LC_ALL } persist :wheel' > /etc/doas.conf

curl https://winhelp2002.mvps.org/hosts.txt > /etc/hosts

trizen opendoas-sudo mawk dashbinsh c-lolcat hyx jdk-temurin jdk17-temurin jdk8-openj9-bin picom-git