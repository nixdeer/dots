# .PROFILE #

# ENV VARS ──────────────────────────────────────────────────────────
export EDITOR='helix'
export _JAVA_AWT_WM_NONREPARENTING=1 # Fixes rendering issue with java apps

# PATH ──────────────────────────────────────────────────────────────
echo $PATH | grep -q "$HOME/.local/bin" || export PATH="$PATH:$HOME/.local/bin"
echo $PATH | grep -q "$HOME/.ghcup/bin" || export PATH="$PATH:$HOME/.ghcup/bin"
echo $PATH | grep -q "$HOME/.cargo/bin" || export PATH="$PATH:$HOME/.cargo/bin"
echo $PATH | grep -q "$HOME/.local/scripts" || export PATH="$PATH:$HOME/.local/scripts"
echo $PATH | grep -q "/usr/local/texlive/2022/bin/x86_64-linux" || export PATH="$PATH:/usr/local/texlive/2022/bin/x86_64-linux"

# X SESSION ─────────────────────────────────────────────────────────
test "$(tty)" = "/dev/tty1" && exec startx


# Added by Toolbox App
export PATH="$PATH:/home/user/.local/share/JetBrains/Toolbox/scripts"

