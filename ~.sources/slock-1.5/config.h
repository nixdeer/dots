/* user and group to drop privileges to */
static const char *user  = "nobody";
static const char *group = "nobody";

static const char *colorname[NUMCOLS] = {
[INIT] =   "black",     /* after initialization */
[INPUT] =  "#222222",   /* during input */
[FAILED] = "#f00c0c",   /* wrong password */
};

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 0;

#define BLUR
/*Set blur radius*/
static const int blurRadius=8;
