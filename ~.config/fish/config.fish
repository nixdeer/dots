# CONFIG.FISH

# PROMPT ────────────────────────────────────────────────────────────
function fish_prompt
    if [ $status -eq 0 ]
        set_color green
    else
        set_color red
    end
    printf '\u23fd %s%s%s \uf0da ' \
    	(set_color cyan) (prompt_pwd) (set_color normal)
end


# MISC ──────────────────────────────────────────────────────────────
set fish_greeting
set fish_color_valid_path

abbr 'xs' 'xsel -i -b'
abbr 'src' 'source ~/.config/fish/config.fish'

alias 'py' 'python3'
alias 'hx' 'helix'
alias 'pamcan' 'pacman' # Yes ... really
alias 'll' 'exa -la --octal-permissions'
alias 'la' 'exa -a'
alias 'ta' 'exa --tree -la --octal-permissions'
alias 'bat' 'bat --wrap never'
